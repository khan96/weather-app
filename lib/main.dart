import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:weather_app/models/weather_model.dart';
import 'package:weather_app/screens/home_page.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Hive.initFlutter();
  Hive.registerAdapter(CityModelAdapter());
  Hive.registerAdapter(WeatherAdapter());
  Hive.registerAdapter(ListWeatherAdapter());
  Hive.registerAdapter(MainAdapter());
  Hive.registerAdapter(WindAdapter());
  Hive.registerAdapter(CityAdapter());
  await Hive.openBox<List<CityModel>>('myBox');
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}
