import 'package:flutter/material.dart';
import 'package:weather_app/models/weather_model.dart';

class DetailsPage extends StatelessWidget {
  final CityModel cityModel;

  const DetailsPage({Key? key, required this.cityModel}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey[800],
        title: const Text('Detailed Information'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: ListView.separated(
          itemCount: cityModel.list!.length,
          itemBuilder: (context, index) => ListTile(
            title: Text(cityModel.list![index].dateTime!),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Temperature: ${cityModel.list![index].main!.temp!}'),
                Text('Feels like: ${cityModel.list![index].main!.feelsLike!}'),
                Text('Wind: ${cityModel.list![index].wind!.speed!}'),
                Text('Humidity: ${cityModel.list![index].main!.humidity!}'),
              ],
            ),
          ),
          separatorBuilder: (BuildContext context, int index) =>
              const Divider(),
        ),
      ),
    );
  }
}
