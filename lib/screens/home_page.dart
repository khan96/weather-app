import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:weather_app/models/weather_model.dart';
import 'package:weather_app/screens/details_page.dart';
import 'package:weather_app/services/bloc/bloc.dart';
import 'package:weather_app/services/bloc/event.dart';
import 'package:weather_app/services/bloc/state.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void dispose() {
    Hive.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Weather App'),
          backgroundColor: Colors.blueGrey[800],
          bottom: const TabBar(
            tabs: [
              Tab(
                icon: Icon(Icons.wb_twighlight),
              ),
              Tab(
                icon: Icon(Icons.web_outlined),
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            BlocProvider(
              create: (BuildContext context) => WeatherBloc(),
              child: const FirstTab(),
            ),
            const SecondTab(),
          ],
        ),
      ),
    );
  }
}

class FirstTab extends StatefulWidget {
  const FirstTab({Key? key}) : super(key: key);

  @override
  State<FirstTab> createState() => _FirstTabState();
}

class _FirstTabState extends State<FirstTab> {
  @override
  void initState() {
    super.initState();
    context.read<WeatherBloc>().add(LoadWeather());
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<WeatherBloc, WeatherState>(
      builder: (BuildContext context, state) => Padding(
        padding: const EdgeInsets.all(10.0),
        child: state is WeatherLoaded
            ? ListView.separated(
                itemCount: state.weatherInfo.length,
                itemBuilder: (context, index) => ListTile(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return DetailsPage(cityModel: state.weatherInfo[index]);
                    }));
                  },
                  trailing: Text(
                      "${state.weatherInfo[index].list!.first.main!.humidity}"),
                  title: Text(state.weatherInfo[index].city!.name!),
                  subtitle: Text(
                      'Temperature: ${state.weatherInfo[index].list!.first.main!.temp}'),
                ),
                separatorBuilder: (BuildContext context, int index) =>
                    const Divider(),
              )
            : const Center(
                child: CircularProgressIndicator(),
              ),
      ),
    );
  }
}

class SecondTab extends StatelessWidget {
  const SecondTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<Box<List<CityModel>>>(
        valueListenable: Hive.box<List<CityModel>>('myBox').listenable(),
        builder: (context, Box<List<CityModel>> box, widget) {
          final cities = box.get('weather');
          return Padding(
            padding: const EdgeInsets.all(10.0),
            child: ListView.separated(
              itemCount: cities!.length,
              itemBuilder: (context, index) => ListTile(
                trailing: Text("${cities[index].list!.first.main!.humidity}"),
                title: Text(cities[index].city!.name!),
                subtitle: Text(
                    'Temperature: ${cities[index].list!.first.main!.temp}'),
              ),
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(),
            ),
          );
        });
  }
}
