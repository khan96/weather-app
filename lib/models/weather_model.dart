import 'package:hive/hive.dart';
part 'weather_model.g.dart';

@HiveType(typeId: 0)
class CityModel {
  @HiveField(0)
  List<ListWeather>? list;
  @HiveField(1)
  City? city;

  CityModel({this.list, this.city});

  CityModel.fromJson(Map<String, dynamic> json) {
    if (json['list'] != null) {
      list = <ListWeather>[];
      json['list'].forEach((v) {
        list!.add(ListWeather.fromJson(v));
      });
    }
    city = json['city'] != null ? City.fromJson(json['city']) : null;
  }
}

@HiveType(typeId: 1)
class ListWeather {
  @HiveField(0)
  Main? main;
  @HiveField(1)
  List<Weather>? weather;
  @HiveField(2)
  Wind? wind;
  @HiveField(3)
  String? dateTime;

  ListWeather({
    this.main,
    this.weather,
    this.wind,
    this.dateTime,
  });

  ListWeather.fromJson(Map<String, dynamic> json) {
    main = json['main'] != null ? new Main.fromJson(json['main']) : null;
    if (json['weather'] != null) {
      weather = <Weather>[];
      json['weather'].forEach((v) {
        weather!.add(new Weather.fromJson(v));
      });
    }
    wind = json['wind'] != null ? new Wind.fromJson(json['wind']) : null;
    dateTime = json['dt_txt'];
  }
}

@HiveType(typeId: 2)
class Main {
  @HiveField(0)
  double? temp;
  @HiveField(1)
  double? feelsLike;
  @HiveField(2)
  int? humidity;

  Main({
    this.temp,
    this.feelsLike,
    this.humidity,
  });

  Main.fromJson(Map<String, dynamic> json) {
    temp = json['temp'].toDouble();
    feelsLike = json['feels_like'].toDouble();
    humidity = json['humidity'];
  }
}

@HiveType(typeId: 3)
class Weather {
  @HiveField(0)
  int? id;
  @HiveField(1)
  String? main;
  @HiveField(2)
  String? description;
  @HiveField(3)
  String? icon;

  Weather({this.id, this.main, this.description, this.icon});

  Weather.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    main = json['main'];
    description = json['description'];
    icon = json['icon'];
  }
}

@HiveType(typeId: 4)
class Wind {
  @HiveField(0)
  double? speed;

  Wind({this.speed});

  Wind.fromJson(Map<String, dynamic> json) {
    speed = json['speed'].toDouble();
  }
}

@HiveType(typeId: 5)
class City {
  @HiveField(0)
  int? id;
  @HiveField(1)
  String? name;

  City({
    this.id,
    this.name,
  });

  City.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }
}
