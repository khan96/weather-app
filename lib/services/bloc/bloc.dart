import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:weather_app/models/weather_model.dart';
import 'package:weather_app/services/bloc/event.dart';
import 'package:weather_app/services/bloc/state.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  WeatherBloc() : super(WeatherLoading()) {
    on<LoadWeather>(_onLoadWeather);
  }

  Future<CityModel> _getWeather(String city) async {
    String url =
        "http://api.openweathermap.org/data/2.5/forecast?q=$city&appid=3a9e70c4670e19b1bada6c5b4a84a6bd";

    var response = await Dio().get(url);
    return CityModel.fromJson(response.data);
  }

  void _onLoadWeather(LoadWeather event, Emitter<WeatherState> emit) async {
    emit(WeatherLoading());

    var cities = [
      'London',
      'Moscow',
      'New York',
      'Tashkent',
      'Paris',
      'Khiva',
      'Namangan',
      'Tokyo',
      'Beijing',
      'Hong Kong',
      'Taipei',
      'Delhi',
      'Islamabad',
      'Minsk',
      'Kyiv',
      'St. Petersburg',
      'Manchester',
      'Berlin',
      'Hamburg',
      'Rome',
    ];

    final weather = [
      for (var c in cities) await _getWeather(c),
    ];

    final box = Hive.box<List<CityModel>>('myBox');
    box.put('weather', weather);
    emit(WeatherLoaded(weatherInfo: weather));
  }
}
