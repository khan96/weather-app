import 'package:equatable/equatable.dart';
import 'package:weather_app/models/weather_model.dart';

abstract class WeatherState extends Equatable {
  const WeatherState();

  @override
  List<Object> get props => [];
}

class WeatherLoading extends WeatherState {}

class WeatherLoaded extends WeatherState {
  final List<CityModel> weatherInfo;

  const WeatherLoaded({this.weatherInfo = const <CityModel>[]});

  @override
  List<Object> get props => [weatherInfo];
}
